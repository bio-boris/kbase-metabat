
package us.kbase.metabat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * <p>Original spec-file type: AlignmentResult</p>
 * <pre>
 * output for run_alignments
 * a list of pandas type dataframes
 * </pre>
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "depth_dict"
})
public class AlignmentResult {

    @JsonProperty("depth_dict")
    private List<List<String>> depthDict;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    @JsonProperty("depth_dict")
    public List<List<String>> getDepthDict() {
        return depthDict;
    }

    @JsonProperty("depth_dict")
    public void setDepthDict(List<List<String>> depthDict) {
        this.depthDict = depthDict;
    }

    public AlignmentResult withDepthDict(List<List<String>> depthDict) {
        this.depthDict = depthDict;
        return this;
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public java.lang.String toString() {
        return ((((("AlignmentResult"+" [depthDict=")+ depthDict)+", additionalProperties=")+ additionalProperties)+"]");
    }

}
